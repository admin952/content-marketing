## Details

<!-- all fields must be completed before moving issue to status:wip --> 

### Content title:

* **Content type:**
* **Content DRI:**
* **MPM DRI:**
* **PMM DRI:**
* **Design DRI:**
* **Campaign:**
* **Buyer's journey stage:**
* **Target persona:**

#### Copy due date:
#### Launch date:

### [eBook Google Doc template >>](https://docs.google.com/document/d/1V944SVbjBxUVMtZiWIS-Baoxc7Ui-gjq3fCYf8tK0D4/edit?usp=sharing) - *make a copy of this template and provide requested copy*

## Asset Brief  

*What is this resource about?*

<!--- Fill this out before submitting -->

*What should readers walk away with?* 

<!--- Fill this out before submitting -->

*Research requirements?*

<!--- Fill this out before submitting -->


## Action items 

* [ ] Add a deadline
* [ ] Assign author
* [ ] Epic created
* [ ] Design issue created
* [ ] Gated MPM issue created
* [ ] Get keyword input from Digital team
* [ ] Write draft
* [ ] SME review 
* [ ] Attach final copy document to design issue
`Close this issue!`

/label  ~"Content Marketing" ~"mktg-status::triage" ~"MPM - Radar" ~"mktg-content"  ~"mktg-inbound"
