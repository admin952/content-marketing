# DETAILS

<!-- Minimum 2 weeks design production time for First PDF draft / 1 week for Content owner review / 1 week for design to make eidts & deliver Final PDF -->

- **Content owner:** 
- **Content title:** 
- **Final copy draft delivery date:** 
- **First PDF draft delivery date:**
- **Requested final PDF delivery date:** 
- **Notes for designer:**

### [Resource draft](#) 

# ACTION ITEMS

- [ ] Final copy shared with the designer `(@content-owner)` 
- [ ] Graphics confirmed (charts, images, etc.) with the designer `(@production-designer)` 
- [ ] First PDF draft delivered `(@production-designer)` 
- [ ] Content owner review `(@content-owner)` 
- [ ] Final PDF delivered `(@production-designer)` 
- [ ] Final review `(@content-owner)` 
- [ ] PDF link shared to gated-content issue `(@production-designer)`


/label ~"design" ~"mktg-status::plan" ~"design-P4" ~"mktg-inbound" ~"mktg-status::triage" ~"Content Marketing"
