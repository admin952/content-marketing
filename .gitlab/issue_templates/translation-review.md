<!-- Use this issue to request a native language speaker to review a translation. [See the handbook](https://about.gitlab.com/handbook/marketing/localization/) for more information about requesting a translation review. -->

STEP ONE: Name this issue: TRANSLATION REVIEW: [Language] + [Asset name]

## Details

- [Link to translated document](#)
- Add document word count 
- Add requested deadline 

Does this document need a technical review? 

- [ ] Yes
- [ ] No

## Workflow

- [ ] Assign to reviewer
- [ ] Complete review with tracked changes 
- [ ] Provide feedback: [Feedback on quality of the translation]
- [ ] Assign back to requestor to address feedback with agency
- [ ] Rquestor to provide updated document and assign back to reviewer
- [ ] Review signs off on document
- [ ] Close this issue! 

/label ~"Localization" ~"mktg-status::triage" ~"mktg-inbound" ~"mktg-content"

