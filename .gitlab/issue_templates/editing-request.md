## Request details

<!-- Depending on the length of the content, editing and review cycles can take 5-10 business days --> 

All fields must be copmleted by requestor 

- Requestor: 
- Date requested:
- Topic: 
- Campaign: 
- Gated content issue: 

### [Draft](attach or link to draft)

## Services needed 

Check all the apply

- [ ] Draft editing
     - [ ] Content has been peer reviewed by teammate or manager
- [ ] Landing page copy writing
- [ ] Landing page copy editing
- [ ] Design 

/label ~"mktg-content" ~"Content Marketing" ~"mktg-status::triage"  ~"mktg-inbound"
