<!-- Purpose of this issue: To write copy for a landing page. -->

## Action Items Checklist
* [ ] Name this issue `Landing Page Copy: <content name>` (ex. Landing Page Copy: GitOps 101 eBook)
* [ ] Add to relevant epic and Set due date based on [timeline guidelines](https://docs.google.com/spreadsheets/d/1RvYLEUJvh9QZJZX1uXiKJPcOOSGd7dEeNneqyA2bt3A/edit#gid=969067029) - note these are recommendations, up for discussion in collaborating with the team.
* [ ] Provide type of Marketo landing page (select one)
   - [ ] Gated Content
   - [ ] Other: 
* [ ] Copy: `add link to copy googledoc - be sure it is editable by all GitLab`
* [ ] Reviewers/Approvers: `@gitlab-handle of relevant individuals`

<!-- DO NOT UPDATE - PROECT MANAGEMENT
/label ~"mktg-status::wip" ~"Content" ~"mktg-inbound"
-->
