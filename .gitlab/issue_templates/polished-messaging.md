MPM to create this issue and name using the following format: `Polished Messaging - Name of Campaign` (then delete this line)

## Purpose

Provide final draft for polished messaging tab in `[campaign name]` in the linked googledoc below.

*Examples from previous campaigns are listed to the right of the spaces to be filled in, as examples.*

## [Link to polished messaging tab]()

`MPM to add direct link to appropriate tab in campaign execution timeline and then delete this note`

*Note to MPM: [this is the overall process template which will be clone for the campaign](https://docs.google.com/spreadsheets/d/1VTrWNX9qfY99b2TnrX93P39aXiRoNnChB6tduTvmysA/edit#gid=1097532784)

/label ~"MPM Integrated" ~"Marketing Programs"

<!-- Please leave the label below on this issue -->
/label ~"Strategic Marketing"  ~"mktg-inbound" ~"mktg-status::triage" ~"Content Marketing" ~"mktg-content"
