<!-- Please leave the label below on this issue -->

## Purpose

Complete the Content section of the Persona/Positioning/Messaging matrix and Content Journey Map for the `Name of Campaign` in the linked google doc below. This will be used to determine:

### Content Messaging

1. High-level, single-sentence message
1. Key messages
1. Related topics
1. Keywords 

### Content Journey

1. Available content (green & linked to the live asset)
1. Work in progress content(yellow & linked to the content epic) 
1. Content that needs to be created (red & not linked) 

`Note: for this first step, we are focusing on only the Manager level persona. We will become more specific as these use cases are built out and as our systems are better able to segment and message with further detail. Content included in the Content Journey Map should be specific to the persona and messaging provided.`

## Ask

- [ ] Complete the content section of the [Persona/Positioning/Messaging Matrix](link) for `name of campaign`
- [ ] Complete the [Content Journey Map](link) 
- [ ] Determine content creation plan & timelines for missing content or content that needs to be updated


/label ~"Content Marketing" ~"mktg-content" ~"mktg-status::triage"  ~"mktg-inbound"
