<!-- Note: This template is for content marketers! If you are not a content marketer and you want to open an issue for a blog 
post, please do so in the www-gitlab.com project (see https://about.gitlab.com/handbook/marketing/blog/#publishing-process for details) -->

## www-gitlab-com tracking 

<!-- Create a corresponding issue in the www-gitlab-com project for this post and add link to issue here -->

:point\_up: **Create your merge request from this issue** 

## Blog post details

* **Author:** 
* **Campaign:** 
* **Target audience:**
* **Target keyword:**
* **CTA:** 

### Summary 

*What is this article about?*

### Key takeaway

*What should readers walk away with?* 

### Outline

1. Key point 1
1. Key point 2
1. Key point 3

## TODO 

* [ ] Assign the issue to the writer 
* [ ] Add a deadline
* [ ] Create tracking issue in www-gitlab-com and cc `@rebecca`
* [ ] SME review of summary and outline 
* [ ] Write draft
* [ ] Get keyword input from Digital team
* [ ] SME review 
* [ ] Move to MR for final review and publishing
`Close this issue!`

/label  ~"Content Marketing" ~"mktg-status::triage"  ~"mktg-inbound" ~"Global Content"
